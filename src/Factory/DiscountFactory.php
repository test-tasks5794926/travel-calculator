<?php

namespace App\Factory;

use App\Domain\Discount;
use App\Domain\DiscountInterface;
use App\Domain\LimitedDiscount;
use App\Domain\NoDiscount;


class DiscountFactory implements DiscountFactoryInterface
{
    /**
     * @todo extract dicount to config or DB...
     */
    private const DISCOUNT_EARLY_MAX_VALUE = 1500;
    private const DISCOUNT_CHILD_MAX_VALUE = 4500;


    public function createChildDiscount(?int $age): DiscountInterface
    {
        return match (true) {
            $age >= 12 && $age <= 17 => new Discount(10),
            $age >= 6 && $age < 12 => new LimitedDiscount(30, self::DISCOUNT_CHILD_MAX_VALUE),
            $age >= 3 && $age < 6 => new Discount(80),
            default => new NoDiscount(),
        };
    }

    public function createEarlyBookingDiscount(?string $paymentDate, string $travelStartDate): DiscountInterface
    {
        $travelStartTime = strtotime($travelStartDate);
        if (null === $paymentDate || $travelStartTime < $paymentTime = strtotime($paymentDate)) {
            return new NoDiscount();
        }

        $startYear = date('Y', $travelStartTime);
        $paymentMonth = date('m', $paymentTime);
        // extract
        $discountPeriods = [
            [
                'start' => $startYear . '-04-01',
                'end' => $startYear . '-09-30',
                'discounts' => [
                    '11' => 7,
                    '12' => 5,
                    '01' => 3,
                ]
            ],
            [
                'start' => $startYear . '-10-01',
                'end' => $startYear . '-01-14',
                'discounts' => [
                    '03' => 7,
                    '04' => 5,
                    '05' => 3,
                ]
            ],
            [
                'start' => $startYear . '-01-15',
                'end' => $startYear . '-03-31',
                'discounts' => [
                    '08' => 7,
                    '09' => 5,
                    '10' => 3,
                ]
            ]
        ];

        foreach ($discountPeriods as $period) {
            $startPeriodTime = strtotime($period['start']);
            $endPeriodTime = strtotime($period['end']);
            if ($travelStartTime >= $startPeriodTime && $travelStartTime <= $endPeriodTime) {
                foreach ($period['discounts'] as $month => $discount) {
                    if ("$paymentMonth" === "$month") {
                        return new LimitedDiscount($discount, self::DISCOUNT_EARLY_MAX_VALUE);
                    }
                }
                break;
            }
        }

        return new NoDiscount();
    }
}
