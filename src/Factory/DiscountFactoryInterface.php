<?php

namespace App\Factory;

use App\Domain\DiscountInterface;

interface DiscountFactoryInterface
{
    public function createChildDiscount(int $age): DiscountInterface;
    public function createEarlyBookingDiscount(?string $paymentDate, string $travelStartDate): DiscountInterface;
}
