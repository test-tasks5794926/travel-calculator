<?php

namespace App\Interfaces;

interface ExecutionOrderInterface
{
    public function getOrder(): int;
}