<?php

namespace App\Domain;

class NoDiscount implements DiscountInterface
{
    public function apply(float $price): float
    {
        return $price;
    }
}