<?php

namespace App\Domain;

class LimitedDiscount extends Discount
{
    protected ?float $maxValue;

    public function __construct(
        float $percentage,
        ?float $maxValue,
        int $order = self::DEFAULT_ORDER,
    ) {
        if ($maxValue && $maxValue < 0) {
            throw new \InvalidArgumentException('Max value must be positive');
        }
        parent::__construct($percentage, $order);
        $this->maxValue = $maxValue;
    }

    protected function calcDiscount($price): float
    {
        $discount = parent::calcDiscount($price);

        return $this->maxValue && $discount > $this->maxValue ? $this->maxValue : $discount;
    }
}