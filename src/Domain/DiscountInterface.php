<?php

namespace App\Domain;

interface DiscountInterface
{
    public function apply(float $price): float;
}