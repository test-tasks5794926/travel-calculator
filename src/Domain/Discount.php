<?php

namespace App\Domain;

use App\Interfaces\ExecutionOrderInterface;

class Discount implements DiscountInterface, ExecutionOrderInterface
{
    protected float $percentage;
    protected int $order;
    public const DEFAULT_ORDER = 0;

    public function __construct(
        float $percentage,
        int $order = self::DEFAULT_ORDER,
    ) {
        if ($percentage < 0 || $percentage > 100) {
            throw new \InvalidArgumentException('Percentage must be between 0 and 100');
        }

        $this->percentage = $percentage;
        $this->order = $order;
    }

    protected function getPercentage(): float
    {
        return $this->percentage / 100;
    }

    protected function calcDiscount($price): float
    {
        return $price * $this->getPercentage();
    }

    public function apply(float $price): float
    {
        return round($price - $this->calcDiscount($price), 2);
    }

    public function getOrder(): int
    {
        return $this->order;
    }
}