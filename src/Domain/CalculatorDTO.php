<?php

namespace App\Domain;

use Symfony\Component\Validator\Constraints as Assert;

class CalculatorDTO
{
    #[Assert\Positive]
    protected float $basePrice;
    #[Assert\Date]
    #[Assert\NotBlank]
    protected string $dateOfBirthday;
    #[Assert\Date]
    protected ?string $paymentDate;
    #[Assert\Date]
    protected string $startDate;

    public function __construct(
        float $basePrice,
        string $dateOfBirthday,
        ?string $paymentDate,
        ?string $startDate,
    ) {
        $this->basePrice = $basePrice;
        $this->dateOfBirthday = $dateOfBirthday;
        $this->paymentDate = $paymentDate;
        $this->startDate = $startDate ?? date('Y-m-d');
    }

    public function getBasePrice(): float
    {
        return $this->basePrice;
    }

    public function getDateOfBirthday(): string
    {
        return $this->dateOfBirthday;
    }

    public function getPaymentDate(): ?string
    {
        return $this->paymentDate;
    }

    public function getStartDate(): string
    {
        return $this->startDate;
    }
}
