<?php

namespace App\Helpers;

class DateHelper
{

    public const DAYS_IN_MONTH = 30;
    public const SECONDS_IN_DAY = 86400;

    public static function calculateAge($birthDate): ?int
    {
        try {
            $birthDate = new \DateTime($birthDate);
        } catch (\Exception) {
            return null;
        }

        $currentDate = new \DateTime();

        return $currentDate->diff($birthDate)->y;
    }
}
