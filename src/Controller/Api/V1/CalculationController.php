<?php

declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Domain\CalculatorDTO;
use App\Services\CalculationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/calculation', name: 'calculation_')]
class CalculationController extends AbstractController
{
    public function __construct(protected CalculationService $calculationService)
    {
    }


    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(#[MapQueryString(validationFailedStatusCode: Response::HTTP_BAD_REQUEST)] CalculatorDTO $data): JsonResponse
    {
        return $this->json([
            'totalPrice' => $this->calculationService->calculatePrice($data),
        ]);
    }
}
