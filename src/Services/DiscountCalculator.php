<?php

namespace App\Services;

use App\Domain\DiscountInterface;
use App\Interfaces\ExecutionOrderInterface;

class DiscountCalculator
{
    /**
     * @var $discounts array<DiscountInterface>
     */
    private array $discounts;
    private bool $discountsSorted;
    public float $basePrice;

    private const MAX_DISCOUNTS_NUMBER = 30;

    public function __construct(
        float $basePrice,
    ) {
        $this->discounts = [];
        $this->discountsSorted = true;
        $this->basePrice = $basePrice;
    }

    /**
     * @throws \LengthException
     */
    public function addDiscount(DiscountInterface $discount): self
    {
        if (count($this->discounts) > self::MAX_DISCOUNTS_NUMBER) {
            throw new \LengthException('Maximum number of discounts allowed');
        }

        $this->discounts[] = $discount;
        $this->discountsSorted = !($discount instanceof ExecutionOrderInterface);

        return $this;
    }

    protected function getDiscounts(): array
    {
        /**
         * @todo make SortedCollection
         */
        if (!$this->discountsSorted) {
            usort($this->discounts, function ($a, $b) {
                if ($a instanceof ExecutionOrderInterface && $b instanceof ExecutionOrderInterface) {
                    return $a->getOrder() - $b->getOrder();
                }

                return 0;
            });
            $this->discountsSorted = true;
        }


        return $this->discounts;
    }

    public function calculate(): float
    {
        $result = $this->basePrice;

        foreach ($this->getDiscounts() as $discount) {
            $result = $discount->apply($result);
        }

        return $result;
    }

}
