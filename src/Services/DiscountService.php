<?php

namespace App\Services;

use App\Domain\CalculatorDTO;
use App\Factory\DiscountFactoryInterface;
use App\Helpers\DateHelper;

class DiscountService
{

    public function __construct(protected DiscountFactoryInterface $discountFactory)
    {
    }


    public function checkDiscounts(CalculatorDTO $data): array
    {
            // The age is considered as of today’s date, not the date of the trip
           return [
               $this->discountFactory->createChildDiscount(DateHelper::calculateAge($data->getDateOfBirthday())),
               $this->discountFactory->createEarlyBookingDiscount($data->getPaymentDate(), $data->getStartDate())
           ];
    }
}
