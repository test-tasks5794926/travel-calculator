<?php

namespace App\Services;

use App\Domain\CalculatorDTO;

class CalculationService
{
    public function __construct(protected DiscountService $discountService)
    {
    }

    public function calculatePrice(CalculatorDTO $data): float
    {
        $discountCalc = new DiscountCalculator($data->getBasePrice());

        $discounts = $this->discountService->checkDiscounts($data);

        foreach ($discounts as $discount) {
            $discountCalc->addDiscount($discount);
        }

        return $discountCalc->calculate();
    }
}
