<?php

namespace App\Tests\Unit\Services;

use App\Domain\Discount;
use App\Domain\DiscountInterface;
use App\Domain\LimitedDiscount;
use App\Services\DiscountCalculator;
use PHPUnit\Framework\TestCase;

final class DiscountCalculatorTest extends TestCase
{

    public function testCalculateDiscount()
    {
        $calculator = new DiscountCalculator(100);
        $calculator->addDiscount(new Discount(20, 3));

        $this->assertEquals(80, $calculator->calculate());
    }

    public function testCalculateMultipleDiscount()
    {
        $calculator = new DiscountCalculator(100);
        $calculator->addDiscount(new Discount(80, 10));
        $calculator->addDiscount(new Discount(5, 3));

        $this->assertEquals(19, $calculator->calculate());
    }

    public function testCalculateWithoutDiscount()
    {
        $calculator = new DiscountCalculator(100);

        $this->assertEquals(100, $calculator->calculate());
    }

    public function testCalculateMaxValueDiscount()
    {
        $calculator = new DiscountCalculator(100);
        $calculator->addDiscount(new LimitedDiscount(80, 50, 1));
        $calculator->addDiscount(new Discount(5, 2));

        $this->assertEquals(47.5, $calculator->calculate());
    }

    public function testCalculateMaxValueDiscountOrdered()
    {
        $calculator = new DiscountCalculator(100);
        $calculator->addDiscount(new LimitedDiscount(80, 50, 2));
        $calculator->addDiscount(new Discount(5, 1));

        $this->assertEquals(45, $calculator->calculate());
    }

    public function testCalculateNotOrderedDiscount()
    {
        $calculator = new DiscountCalculator(100);
        $calculator->addDiscount(new NotOrderedDiscount(80));
        $calculator->addDiscount(new NotOrderedDiscount(5));

        $this->assertEquals(19, $calculator->calculate());
    }
}

final class NotOrderedDiscount implements DiscountInterface
{
    protected float $percentage;

    public function __construct(
        float $percentage,
    ) {
        $this->percentage = $percentage;
    }

    protected function getPercentage(): float
    {
        return $this->percentage / 100;
    }

    protected function calcDiscount($price): float
    {
       return $price * $this->getPercentage();
    }

    public function apply(float $price): float
    {
        return round($price - $this->calcDiscount($price), 2);
    }
}