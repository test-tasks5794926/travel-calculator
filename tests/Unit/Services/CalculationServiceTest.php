<?php

namespace App\Tests\Unit\Services;

use App\Domain\CalculatorDTO;
use App\Factory\DiscountFactory;
use App\Services\CalculationService;
use App\Services\DiscountService;
use PHPUnit\Framework\TestCase;

class CalculationServiceTest extends TestCase
{
    private CalculationService $calculationService;

    protected function setUp(): void
    {
        $this->calculationService = new CalculationService(new DiscountService(new DiscountFactory()));
    }

    /**
     * @dataProvider calculatePriceProvider
     */
    public function testCalculatePrice($calc, $expectedPrice): void
    {
        $this->assertEquals($expectedPrice, $this->calculationService->calculatePrice($calc));
    }

    public function calculatePriceProvider(): array
    {
        return [
            [new CalculatorDTO(1000, '2020-01-01', null, '2024-01-01'), 200],
            [new CalculatorDTO(1000, '2020-01-01', '2023-11-30', '2024-04-13'), 186],
            [new CalculatorDTO(1000, '2001-01-01', '2023-11-30', '2024-04-13'), 930],
            [new CalculatorDTO(1000, '2024-01-01', '2023-01-14', '2024-04-13'), 970],
            [new CalculatorDTO(20000, '2018-01-01', '2023-01-14', '2024-04-13'), 15035],
            //...
        ];
    }

}
