<?php

namespace App\Tests\Unit\Factory;

use App\Domain\DiscountInterface;
use App\Factory\DiscountFactory;
use App\Factory\DiscountFactoryInterface;
use PHPUnit\Framework\TestCase;

class DiscountFactoryTest extends TestCase
{

    private DiscountFactoryInterface $discountFactory;

    protected function setUp(): void
    {
        $this->discountFactory = new DiscountFactory();
    }

    /**
     * @dataProvider childDataProvider
     */
    public function testChild($age, $price, $expected)
    {
        $discount = $this->discountFactory->createChildDiscount($age);
        $this->assertInstanceOf(DiscountInterface::class, $discount);
        $this->assertEquals($expected, $discount->apply($price));
    }

    /**
     * @dataProvider earlyDataProvider
     */
    public function testEarly($travelStartDate, $paymentDate, $price, $expected)
    {
        $discount = $this->discountFactory->createEarlyBookingDiscount($paymentDate, $travelStartDate);
        $this->assertInstanceOf(DiscountInterface::class, $discount);
        $this->assertEquals($expected, $discount->apply($price));

    }

    public function earlyDataProvider(): array
    {
        return [
            ['2023-07-10', '2022-11-15', 1000, 930],
            ['2023-05-15', '2022-12-02', 1000, 950],
            ['2024-03-20', '2023-10-01', 1000, 970],
            ['2022-12-15', '2022-12-01', 1000, 1000],
            ['2025-01-01', null, 1000, 1000],
        ];
    }
    public function childDataProvider(): array
    {
        return [
            [18, 1000, 1000],
            [14, 1000, 900],
            [12, 1000, 900],
            [17, 1000, 900],
            [6, 1000, 700],
            [11, 1000, 700],
            [9, 20000, 15500],
            [4, 1000, 200],
            [3, 1000, 200],
            [1, 1000, 1000],
            [2, 1000, 1000],
        ];
    }

}
