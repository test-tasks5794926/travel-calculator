<?php

namespace App\Tests\Unit\Domain;

use App\Domain\Discount;
use App\Domain\LimitedDiscount;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class DiscountTest extends TestCase
{
    private const BASE_PRICE = 100;
    private const BASE_ORDER = 1;

    public function testDiscount()
    {
        $discount = new Discount(10);

        $this->assertEquals(90, $discount->apply(self::BASE_PRICE));
    }

    public function testSmallMaxValueDiscount()
    {
        $discount = new LimitedDiscount(90, 50);

        $this->assertEquals(self::BASE_PRICE - 50, $discount->apply(self::BASE_PRICE));
    }

    public function testMaxValueDiscount()
    {
        $discount = new LimitedDiscount(90, 1000);

        $this->assertEquals(10, $discount->apply(self::BASE_PRICE));
    }

    public function testRoundPrecisionDiscount()
    {
        $price = 99.5;
        $percentage = 3;
        $expectedPrice = round($price - ($price * $percentage / 100), 2);
        $discount = new Discount($percentage);

        $this->assertEquals("$expectedPrice", "{$discount->apply($price)}");
    }

    public function testGetOrder()
    {
        $this->assertEquals(self::BASE_ORDER, (new Discount(2, self::BASE_ORDER))->getOrder());
    }

    public function testDefaultOrderDiscount()
    {
        $this->assertEquals(Discount::DEFAULT_ORDER, (new Discount(1))->getOrder());
    }

    public function testTryCreateNegativeDiscount()
    {
        $this->expectException(\InvalidArgumentException::class);
        new Discount(-1);
    }

    public function testTryCreateOverDiscount()
    {
        $this->expectException(\InvalidArgumentException::class);
        new Discount(101);
    }

    public function testTryCreateNegativeMaxValue()
    {
        $this->expectException(\InvalidArgumentException::class);
        new LimitedDiscount(1, -1);
    }

    public function testTryCreateNullMaxValue()
    {
        new Discount(1);

        Assert::assertFalse(isset($discount));
    }

}
